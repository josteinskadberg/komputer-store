

# Project details 

project is written in javascipt /html / css 
deployed to heroku:
https://my-komputer-store.herokuapp.com/

## Contributors ## 

Jostein Skadberg (ME)

# Komputer Store 

This static website compiles your imaginary bank, job and computer store into one experience. You can grind your job, take a loan and buy computers with the click of a mouse. 

## Requirements 

***Bank***
* Displays your current balance. 
* Button for loan request.
    * Loan amount is caped at twice the bank balance.
    * One cannot get more than one loan before buying a computer.
    * You must pay down you loan before you can get another one.
* Displays the outstanding loan if loan exists. 

***Work***
* Pay displays the current payment in NOK.  
* Bank button; Transfers the payment to the bank.
    * If a loan exists, 10% of the payment goes to pay down the loan. 
* Work button increase your payment by 100 NOK.
* Repay Loan button is visible if a loan exists.   
    * On click the payment is used to pay down the loan.
    * If payment is larger than loan the remainder goes to the bank balance

**Computers**

Uses a provided komputer store API  
Computers are displayed with:

* title
* description
* spesifications
* picture 
* price 

Clicking the buy now button will make a purchase of the computer. The fake money will automatically be drawn from your bank balance.

If there is insufficient funds, the purchase will not go through.
