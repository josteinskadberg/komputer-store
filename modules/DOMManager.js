import transactions from "./transactionLogic.js"

let products = []

const imageMissingURL = "https://upload.wikimedia.org/wikipedia/commons/b/b1/Missing-image-232x150.png"
const baseUrl = "https://noroff-komputer-store-api.herokuapp.com/"

const balanceAmountElement = document.getElementById("balance-amount");
const loanAmountElement = document.getElementById("loan-amount");
const loanButton = document.getElementById("loan-button");
const payAmountElement = document.getElementById("pay-amount");
const workButton = document.getElementById("work-button");
const bankButton = document.getElementById("bank-button");
const payLoanButton = document.getElementById("pay-loan-button");
const computerSelection = document.getElementById("computer-selection");
const purchaseButton = document.getElementById("purchase-computer");
const specContainer = document.getElementById("spec-container")
const computerPriceElement = document.getElementById("computer-price")
const computerTitle = document.getElementById("computer-title")
const computerImgElement = document.getElementById("computer-img")
const computerDescription = document.getElementById("computer-description")


//set products and initiate functions for populating the website with products 
export const initiate = (productData) => {
    products = productData
    addProductsToMenu(products.getAllProducts())
}

//add products to select menu 
const addProductsToMenu = (products) => {
    products.forEach(product => {
        addProductToMenu(product);
    });
    handleProductChange()
}

// toggle visibility of elements assoisiated with active/inactive loan
function toggleLoanVisibility() {
    let elements = document.getElementsByClassName("loan-dependent");
    for (var i = 0; i < elements.length; i++) {
        elements[i].style.display = elements[i].style.display == 'inline' ? 'none' : 'inline';
    }
}

//generate option element and append select 
const addProductToMenu = (product) => {
    const computerOption = document.createElement("option");
    computerOption.value = product.id
    computerOption.appendChild(document.createTextNode(product.title))
    computerSelection.appendChild(computerOption)

}

//update product information
const handleProductChange = () => {
    const selected = products.getProduct(computerSelection.selectedIndex)
    specContainer.replaceChild(generateSpecs(selected), specContainer.childNodes[0])
    computerImgElement.src = baseUrl + selected.image
    computerPriceElement.innerText = formatToNOK(selected.price)
    computerDescription.innerText = selected.description
    computerTitle.innerHTML = selected.title
}

//returns ul element with li for each product spec 
const generateSpecs = (product) => {
    const specs = document.createElement("ul")
    product.specs.forEach(spec => {
        const specElement = document.createElement("li")
        specElement.innerText = spec
        specs.appendChild(specElement)
    })
    return specs
}

//Format number to Norwegian krone format 
const formatToNOK = (number) => {
    return new Intl.NumberFormat("no-NO", { style: "currency", currency: "NOK" }).format(number)
}

//Handle click of pay button (see transactionLogic.js)
const handlePayout = () => {
    transactions.payout()
    const updatedBalance = {
        "payBalance": transactions.getPay(),
        "bankBalance": transactions.getBank(),
        "loanBalance": transactions.getLoan()
    }
    updateEconomy(updatedBalance)
}

//Handle click of work button (see transactionLogic.js)
const handleWork = () => {
    transactions.work()
    const updatedBalance = {
        "payBalance": transactions.getPay(),
    }
    updateEconomy(updatedBalance)
}

//Handle click of work button (see transactionLogic.js)
const handlePayLoan = () => {
    transactions.payLoan()

    const updatedBalance = {
        "payBalance": transactions.getPay(),
        "bankBalance": transactions.getBank(),
        "loanBalance": transactions.getLoan()
    }
    updateEconomy(updatedBalance)

    if (transactions.getLoan() === 0) {
        toggleLoanVisibility()
    }
}

//Handle click of purchase button (see transactionLogic.js)
const handleProductPurchase = () => {
    const selected = products.getProduct(computerSelection.selectedIndex)
    if (transactions.purchase(selected)) {

        const updatedBalance = {
            "bankBalance": transactions.getBank(),
        }
        updateEconomy(updatedBalance)

        alert(`Congratulations you have bougth a ${selected.title}`)
    }
    else if (transactions.getPay() >= selected.price) {
        alert("You lack money on your bank account. Maybe you should bank your paycheck first.")
    }
    else {
        let difference = selected.price - transactions.getBank()
        alert(`You lack ${formatToNOK(difference)} NOK to buy this computer. Go do some work or take up a loan`)
    }

}

//Handle click of "get loan" button. Alert message based on if request is successful (see transactionLogic.js)
const handleLoanRequest = () => {
    let amount_requested = prompt("Please provide the sum you want to loan:").replace(/\s/g, "")
    let validInput = amount_requested.match(/^[1-9][0-9]*$/) != null

    if (!validInput) {
        alert(`${amount_requested} is not a valid input, please input a number`)
    }
    else {
        amount_requested = parseInt(amount_requested, 10)
        if (transactions.takeLoan(amount_requested)) {

            const updatedBalance = {
                "bankBalance": transactions.getBank(),
                "loanBalance": transactions.getLoan()
            }
            toggleLoanVisibility()
            updateEconomy(updatedBalance)
            alert("Congratulations your loan request was approved")

        }
        else {
            alert("LOAN REQUEST DENIED\n\nLoan requirments: Your loan request cannot exceed twice the amount of your current bank balance."
                + "\n\nYou need to pay down the existing loan before you apply for a new one.\n\n"
                + "You also need to hva bougth a computer before you can make a loan request."
                + " This is true for your first loan request and each new request after the first one.")
        }
    }
}

/**Updates text given balancetype in DOM
 * @param {string : number} */
export const updateEconomy = (updatatedBalance) => {

    for (let updated in updatatedBalance) {
        switch (updated) {
            case "payBalance":
                payAmountElement.innerText = formatToNOK(updatatedBalance[updated])
                break;

            case "bankBalance":
                balanceAmountElement.innerText = formatToNOK(updatatedBalance[updated])
                break;

            case "loanBalance":
                loanAmountElement.innerText = formatToNOK(updatatedBalance[updated])
                break;

            default:
                break;

        }
    }

}

//set event listeners 
workButton.addEventListener("click", handleWork)
bankButton.addEventListener("click", handlePayout)
purchaseButton.addEventListener("click", handleProductPurchase)
loanButton.addEventListener("click", handleLoanRequest)
payLoanButton.addEventListener("click", handlePayLoan)
computerSelection.addEventListener("change", handleProductChange);


//changes to default image if error with loading image
computerImgElement.addEventListener("error", (event) => {
    console.log("Could not show image")
    computerImgElement.src = imageMissingURL
})