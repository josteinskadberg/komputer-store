// IFFE with methods for changing pay balance, bank balance and loan balance
const transactions = (() => {
    let currentPay = 0.0
    let currentBalance = 0.0
    let currentLoan = 0.0
    let loanPurchaseRestraint = true //purchase product before loan request

    return {

        work: () => {
            currentPay += 100
        },

        /* Transfer money from pay balance to bank balance. 
            - if loan exists 10% is transfered to pay down loan
            - if the 10% is larger than loan eccess money transefers to bank balance*/
        payout: () => {
            if (currentLoan > 0) {
                const loanCover = currentPay * 0.1
                if (loanCover > currentLoan) {
                    currentPay -= currentLoan
                    currentLoan = 0
                }
                else {
                    currentLoan -= loanCover
                }
            }
            currentBalance += currentPay
            currentPay = 0
        },

        //Transfers money to loan and bank balance given loan conditions forfilled
        takeLoan: (amount) => {
            if (amount <= (currentBalance * 2) && !loanPurchaseRestraint && currentLoan == 0) {
                currentLoan = amount
                currentBalance += amount
                loanPurchaseRestraint = true
                return true
            }
            else {
                return false
            }
        },

        //Tranfers money from pay balance to loan balance. Excess money transfers to bank balance.  
        payLoan: () => {
            if (currentPay > currentLoan) {
                currentBalance += currentPay - currentLoan
                currentLoan = 0
            }
            else {
                currentLoan -= currentPay
            }
            currentPay = 0
        },

        getPay: () => {
            return currentPay;
        },

        getBank: () => {
            return currentBalance;
        },
        getLoan: () => {
            return currentLoan;
        },

        //transfers money from bank balance
        purchase: (item) => {
            if (currentBalance >= item.price) {
                currentBalance -= item.price
                loanPurchaseRestraint = false
                return true
            }
            return false
        }

    }
})();

export default transactions