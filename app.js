import { initiate } from "./modules/DOMManager.js";
export let products


const fetchJSON = async (url) => {
    const response = await fetch(url)
    const data = await response.json()
    return data
}

const main = ()=>{
    const productURL = "https://noroff-komputer-store-api.herokuapp.com/computers"
    fetchJSON(productURL).then(data =>
        products = (() => {
            let currentStock = data
            return {
                getAllProducts: () => {
                    return currentStock
                },
                getProduct: (index) => {
                    return currentStock[index]
                }
            }
        })())
        .then(() => initiate(products))

}

main()